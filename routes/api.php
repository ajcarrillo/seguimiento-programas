<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/areas-responsables', 'API\AreaResponsableController@index')->name('api.areas.responsables')->middleware('auth:api');
Route::get('/responsables', 'API\ResponsableController@index')->name('api.responsables')->middleware('auth:api');
Route::get('/beneficiarios', 'API\BeneficiarioController@index')->name('api.beneficiarios')->middleware('auth:api');
Route::get('/partidas-presupuestales', 'API\PartidaPresupuestalController@index')->name('api.partidas.presupuestales')->middleware('auth:api');

Route::post('/programas/{programa}/tipo-apoyos', 'API\ProgramaTipoApoyoController@store')->name('api.tipo.apoyos')->middleware('auth:api');

Route::patch('/meta/{meta}/avance', 'API\UpdateAvanceMetaController@update')->name('api.update.avance.meta')->middleware('auth:api');

Route::resource('/actividades/{actividad}/metas', 'API\MetaController', [
    'parameters' => [ 'metas' => 'meta' ],
    'only'       => [ 'store', 'update' ],
    'names'      => [
        'store'  => 'api.metas.store',
        'update' => 'api.metas.update',
    ],
])->middleware('auth:api');

Route::resource('acuerdos/{acuerdo}/comentarios', 'API\AcuerdoComentarioController', [
    'parameters' => [ 'comentarios' => 'comentario' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'api.acuerdo.comentarios.index',
        'store'  => 'api.acuerdo.comentarios.store',
        'show'   => 'api.acuerdo.comentarios.show',
        'update' => 'api.acuerdo.comentarios.update',
    ],
]);

Route::resource('programas/{programa}/actividades', 'API\ProgramaActividadController', [
    'parameters' => [ 'actividades' => 'actividad' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'actividades.index',
        'store'  => 'actividades.store',
        'show'   => 'actividades.show',
        'update' => 'actividades.update',
    ],
])->middleware('auth:api');

Route::resource('programas', 'API\ProgramaController', [
    'parameters' => [ 'programas' => 'programa' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'api.programas.index',
        'store'  => 'api.programas.store',
        'show'   => 'api.programas.show',
        'update' => 'api.programas.update',
    ],
])->middleware('auth:api');

Route::resource('reuniones', 'API\ReunionController', [
    'parameters' => [ 'reuniones' => 'reunion' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'api.reuniones.index',
        'store'  => 'api.reuniones.store',
        'show'   => 'api.reuniones.show',
        'update' => 'api.reuniones.update',
    ],
])->middleware('auth:api');

Route::resource('reuniones/{reunion}/acuerdos', 'API\AcuerdoController', [
    'parameters' => [ 'acuerdos' => 'acuerdo' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'api.acuerdos.index',
        'store'  => 'api.acuerdos.store',
        'show'   => 'api.acuerdos.show',
        'update' => 'api.acuerdos.update',
    ],
])->middleware('auth:api');

Route::resource('reuniones/{reunion}/comentarios', 'API\ComentarioController', [
    'parameters' => [ 'comentarios' => 'comentario' ],
    'only'       => [ 'index', 'store', 'show', 'update' ],
    'names'      => [
        'index'  => 'api.comentarios.index',
        'store'  => 'api.comentarios.store',
        'show'   => 'api.comentarios.show',
        'update' => 'api.comentarios.update',
    ],
])->middleware('auth:api');