<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'prefix' => '/administracion', 'middleware' => [ 'auth' ] ], function () {
    Route::get('/areas', 'AreaResponsableController@index')->name('areas.index');
    Route::post('/areas', 'AreaResponsableController@store')->name('areas.store');
    Route::patch('/areas/{area}', 'AreaResponsableController@update')->name('areas.update');
    Route::delete('/areas/{area}', 'AreaResponsableController@delete')->name('areas.delete');

    Route::get('/beneficiarios', 'BeneficiarioController@index')->name('beneficiarios.index');
    Route::post('/beneficiarios', 'BeneficiarioController@store')->name('beneficiarios.store');
    Route::patch('/beneficiarios/{beneficiario}', 'BeneficiarioController@update')->name('beneficiarios.update');
    Route::delete('/beneficiarios/{beneficiario}', 'BeneficiarioController@delete')->name('beneficiarios.delete');

    Route::get('/partidas', 'PartidasPresupuestalesController@index')->name('partidas.index');
    Route::post('/partidas', 'PartidasPresupuestalesController@store')->name('partidas.store');
    Route::patch('/partidas/{partida}', 'PartidasPresupuestalesController@update')->name('partidas.update');
    Route::delete('/partidas/{partida}', 'PartidasPresupuestalesController@delete')->name('partidas.delete');

    Route::get('/responsables', 'ResponsableController@index')->name('responsables.index');
    Route::post('/responsables', 'ResponsableController@store')->name('responsables.store');
    Route::patch('/responsables/{responsable}', 'ResponsableController@update')->name('responsables.update');
    Route::delete('/responsables/{responsable}', 'ResponsableController@delete')->name('responsables.delete');

    Route::get('/usuarios', 'UsuarioController@index')->name('usuarios.index');
    Route::get('/usuarios/nuevo', 'UsuarioController@create')->name('usuarios.create');
    Route::post('/usuarios', 'UsuarioController@store')->name('usuarios.store');
    Route::get('/usuarios/{user}/editar', 'UsuarioController@edit')->name('usuarios.edit');
    Route::patch('/usuarios/{user}', 'UsuarioController@update')->name('usuarios.update');

    Route::patch('/usuarios/{user}/activar', 'ActivarUsuarioController@update')->name('usuarios.activar');
    Route::delete('/usuarios/{user}/desactivar', 'ActivarUsuarioController@delete')->name('usuarios.desactivar');
});

Route::patch('/ministraciones/{ministracion}/actualiza-presupuesto', 'ActualizaPresupuestoMinistracionController@update')
    ->name('ministraciones.presupuesto.update')
    ->middleware([ 'auth' ]);

Route::get('/programas/{programa}/actividades', 'ProgramaActividadController@index')
    ->name('programa.actividades.index')
    ->middleware([ 'auth' ]);

Route::group([ 'prefix' => '/programas/{programa}/ministraciones', 'middleware' => [ 'auth' ] ], function () {
    Route::get('/', 'ProgramaMinistracionController@index')->name('programa.ministraciones.index');
    Route::post('/', 'ProgramaMinistracionController@store')->name('programa.ministraciones.store');
    Route::patch('/{ministracion}', 'ProgramaMinistracionController@update')->name('programa.ministraciones.update');
    Route::delete('/{ministracion}', 'ProgramaMinistracionController@destroy')->name('programa.ministraciones.destroy');
});

Route::get('/programas', 'ProgramaController@index')
    ->name('programas.index')
    ->middleware([ 'auth' ]);

Route::get('/reuniones', 'ReunionController@index')
    ->name('reuniones.index')
    ->middleware([ 'auth' ]);
