let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/start.js', 'public/js')
    .js('resources/assets/js/programas.js', 'public/js')
    .js('resources/assets/js/programa_actividades.js', 'public/js')
    .js('resources/assets/js/reuniones.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/adminlte.scss', 'public/css')
    .sass('resources/assets/sass/circle.scss', 'public/css')
    .browserSync({
        proxy: 'acuerdos.test',
        files: [
            'app/**/*',
            'resources/views/**/*',
            'resources/assets/**/*',
            'routes/**/*'
        ]
    });

if (mix.inProduction()) {
    mix.version();
}