import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const moduleReuniones = {
    namespaced: true,
    state: {
        reuniones: [],
        reunion: {}
    },
    mutations: {
        reuniones(state, value) {
            state.reuniones = value
        },
        createReunion(state, nuevaReunion) {
            state.reuniones.push(nuevaReunion);
        },
        createReunionComentario(state, nuevoComentario) {
            let reunionIndex = state.reuniones.findIndex(reunion => reunion.id == nuevoComentario.commentable_id);
            state.reuniones[reunionIndex].comments.push(nuevoComentario);
        },
        createAcuerdo(state, nuevoAcuerdo) {
            let reunionIndex = state.reuniones.findIndex(reunion => reunion.id == nuevoAcuerdo.reunion_id);
            state.reuniones[reunionIndex].acuerdos.push(nuevoAcuerdo);
        },
        createAcuerdoComentario(state, payload) {
            let reunionIndex = state.reuniones.findIndex(reunion => reunion.id == payload.reunionId);
            let acuerdoIndex = state.reuniones[reunionIndex].acuerdos.findIndex(acuerdo => acuerdo.id == payload.nuevoComentario.commentable_id);
            state.reuniones[reunionIndex].acuerdos[acuerdoIndex].comments.push(payload.nuevoComentario);
        }
    },
    actions: {
        createReunion(context, nuevaReunion) {
            axios.post(route('api.reuniones.store'), {
                reunion: nuevaReunion
            })
                .then(res => {
                    context.commit('createReunion', res.data.reunion);
                })
                .catch(err => {
                    console.log(err.response);
                })
        },
        createAcuerdo(context, nuevoAcuerdo) {
            axios.post(route('api.acuerdos.store', nuevoAcuerdo.reunion_id), {
                acuerdo: nuevoAcuerdo
            })
                .then(res => {
                    context.commit('createAcuerdo', res.data.acuerdo);
                })
                .catch(err => {
                    console.log(err.response);
                })
        },
        createReunionComentario(context, nuevoComentario) {
            axios.post(route('api.comentarios.store', nuevoComentario.reunion_id), {
                comentario: nuevoComentario.comentario,
            })
                .then(res => {
                    context.commit('createReunionComentario', res.data.comment);
                })
                .catch(err => {
                    console.log(err.response);
                });
        },
        createAcuerdoComentario(context, payload) {
            // nuevoComentario.reunion_id = acuerdo_id
            axios.post(route('api.acuerdo.comentarios.store', payload.comentario.reunion_id), {
                comentario: payload.comentario.comentario,
            })
                .then(res => {
                    context.commit('createAcuerdoComentario', {reunionId: payload.reunion_id, nuevoComentario: res.data.comment});
                })
                .catch(err => {
                    console.log(err.response);
                });
        }
    }
};

export default new Vuex.Store({
    modules: {
        reuniones: moduleReuniones
    }
});