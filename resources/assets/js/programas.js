window.swal = require('sweetalert2');
window.Vue = require('vue');

import Index from './views/programas/Index';
import Notifications from 'vue-notification'

Vue.use(Notifications);

const app = new Vue({
    el: '#app',
    components: {Index}
});