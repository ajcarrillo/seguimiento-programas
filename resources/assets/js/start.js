window.Vue = require('vue');

import Start from './views/Start';

Vue.use(VueCharts);

const app = new Vue({
    el: '#app',
    components: {Start}
});