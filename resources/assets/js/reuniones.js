window.swal = require('sweetalert2');
window.Vue = require('vue');

import Index from './views/reuniones/Index';
import Notifications from 'vue-notification'
import store from './store';

Vue.use(Notifications);

const app = new Vue({
    el: '#app',
    store,
    components: {Index}
});