@foreach(menu() as $menu)
	@if(!empty($menu['submenus']))
		@if(count(array_intersect($userGroups, $menu['groups'])))
			<li class="nav-item has-treeview menu">
				<a href="#" class="nav-link">
					<i class="nav-icon fa fa-link"></i>
					<p>
						{{ $menu['nombre'] }}
						<i class="right fa fa-angle-left"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					@foreach($menu['submenus'] as $submenu)
						@if(count(array_intersect($userGroups, $submenu['groups'])))
							<li class="nav-item">
								<a href="{{ $submenu['url'] }}" class="nav-link">
									<i class="fa fa-circle-o"></i>
									<p>{{ $submenu['nombre'] }}</p>
								</a>
							</li>
						@endif
					@endforeach
				</ul>
			</li>
		@endif
	@else
		@if(count(array_intersect(getUserGroups(), $menu['groups'])))
			<li class="nav-item">
				<a href="{{ $menu['url'] }}" class="nav-link">
					<i class="fa fa-circle-o nav-icon"></i>
					<p>{{ $menu['nombre'] }}</p>
				</a>
			</li>
		@endif
	@endif
@endforeach

