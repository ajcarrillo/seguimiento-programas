@extends('layouts.app')

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Dashboard v3</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Dashboard v3</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						header
					</div>
					<div class="card-body">
						<h4 class="card-title">title</h4>
						<p class="card-text">text</p>
						<a href="#" class="btn btn-primary">button</a>
					</div>
					<div class="card-footer text-muted">
						footer
					</div>
				</div>
			</div>
		</div>
	</div>
@stop