@extends('layouts.app')

@section('extra-scripts')
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Minstraciones</h1>
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h1 class="card-title">Nueva ministración</h1>
					</div>
					<div class="card-body">
						{!! Form::open(['route'=>['programa.ministraciones.store', $programa->id], 'method'=>'post']) !!}
						<div class="form-row">
							<div class="form-group col">
								{!! Form::label('presupuesto', 'Presupuesto:', ['class'=>'control-label']) !!}
								{!! Form::number('presupuesto', NULL, ['class'=>'form-control text-right', 'required', 'min'=>.01, 'step'=>.01]) !!}
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								{!! Form::label('ingreso_sefiplan', 'Ingreso Sefiplan:', ['class'=>'control-label']) !!}
								{!! Form::date('ingreso_sefiplan', NULL, ['class'=>'form-control text-right', 'required']) !!}
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								{!! Form::label('ingreso_seq', 'Ingreso Sefiplan:', ['class'=>'control-label']) !!}
								{!! Form::date('ingreso_seq', NULL, ['class'=>'form-control text-right', 'required']) !!}
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								{!! Form::label('observaciones', 'Observaciones:', ['class'=>'control-label']) !!}
								{!! Form::textarea('observaciones', NULL, ['class'=>'form-control', 'rows'=>'3']) !!}
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<button type="submit" class="btn btn-success">Guardar</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h1 class="card-title">{{ $programa->descripcion }}</h1>
					</div>
					<div class="card-body table-responsive p-0">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Presupuesto</th>
									<th>Ingreso Sefiplan</th>
									<th>Ingreso SEQ</th>
									<th>Observaciones</th>
									<th>Eliminar</th>
								</tr>
							</thead>
							<tbody>
								@forelse($ministraciones as $ministracion)
									<tr>
										<td style="padding-top: 1.2rem">{{ $loop->iteration }}</td>
										<td>
											{!! Form::open(['route'=>['ministraciones.presupuesto.update', $ministracion->id], 'method'=>'patch']) !!}
											{!! Form::number('presupuesto', $ministracion->presupuesto, ['class'=>'form-control text-right', 'required', 'min'=>.01, 'step'=>.01]) !!}
											{!! Form::close() !!}
										</td>
										<td style="padding-top: 1.2rem">{{ $ministracion->ingreso_sefiplan }}</td>
										<td style="padding-top: 1.2rem">{{ $ministracion->ingreso_seq }}</td>
										<td style="padding-top: 1.2rem">{{ $ministracion->observaciones }}</td>
										<td>
											{!! Form::open(['route'=>['programa.ministraciones.destroy', $programa->id, $ministracion->id], 'class'=>'form-horizontal', 'method'=>'delete']) !!}
											<button class="btn btn-link text-danger" type="submit">
												<i class="fa fa-trash"></i>
												Eliminar
											</button>
											{!! Form::close() !!}
										</td>
									</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop