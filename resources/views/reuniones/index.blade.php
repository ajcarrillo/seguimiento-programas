@extends('layouts.app')

@section('extra-scripts')
	@routes
	<script src="{{ mix('js/reuniones.js') }}" defer></script>
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Reuniones</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
@stop

@section('content')
	<section id="app">
		<index :re="{{ json_encode($reuniones) }}"></index>
	</section>
@stop