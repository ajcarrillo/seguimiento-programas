<div class="form-row">
	<div class="form-group col">
		{!! Form::label('name', 'Nombre:', ['class'=>'control-label']) !!}
		{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
<div class="form-row">
	<div class="form-group col">
		{!! Form::label('username', 'Usuario:', ['class'=>'control-label']) !!}
		{!! Form::text('username', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
<div class="form-row">
	<div class="form-group col">
		{!! Form::label('email', 'Email:', ['class'=>'control-label']) !!}
		{!! Form::email('email', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
@includeWhen($isCreate, 'administracion.usuarios._password_field')
<div class="form-row">
	<div class="form-group col">
		{!! Form::label('groups', 'Grupos:', ['class'=>'control-label']) !!}
		{!! Form::select('groups[]', $groups, NULL, ['class'=>'form-control','required', 'multiple'=>'multiple']) !!}
	</div>
</div>
<div class="form-row">
	<div class="form-group col">
		{!! Form::label('programas', 'Programas:', ['class'=>'control-label']) !!}
		{!! Form::select('programas[]', $programas, NULL, ['class'=>'form-control', 'multiple'=>'multiple']) !!}
	</div>
</div>
