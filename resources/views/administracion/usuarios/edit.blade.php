@extends('layouts.app')

@section('extra-scripts')
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Actualizar usuario</h1>
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 offset-md-3">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card card-primary card-outline">
					<div class="card-body">
						{!! Form::model($user, ['route'=>['usuarios.update', $user->id], 'class'=>'form-horizontal', 'method'=>'patch', 'id'=>'user-update-form']) !!}
						@include('administracion.usuarios._user_form')
						{!! Form::close() !!}
					</div>
					<div class="card-footer">
						<button class="btn btn-success" form="user-update-form">Actualizar</button>
						<a href="{{ route('usuarios.index') }}" class="btn btn-default">Regresar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop