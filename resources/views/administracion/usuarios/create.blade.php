@extends('layouts.app')

@section('extra-scripts')
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Nuevo usuario</h1>
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 offset-md-3">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<div class="card card-primary card-outline">
					<div class="card-body">
						{!! Form::open(['route'=>['usuarios.store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'user-create-form']) !!}
						@include('administracion.usuarios._user_form')
						{!! Form::close() !!}
					</div>
					<div class="card-footer">
						<button class="btn btn-success" form="user-create-form">Guardar</button>
						<a href="{{ route('usuarios.index') }}" class="btn btn-default">Regresar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop