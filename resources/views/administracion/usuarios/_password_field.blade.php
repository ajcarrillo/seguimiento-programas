<div class="form-row">
	<div class="form-group col">
		{!! Form::label('password', 'Contraseña:', ['class'=>'control-label']) !!}
		{!! Form::password('password', ['class'=>'form-control', 'required']) !!}
	</div>
</div>