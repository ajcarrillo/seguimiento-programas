@extends('layouts.app')

@section('extra-scripts')
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Usuarios</h1>
			</div>
		</div>
	</div>
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h1 class="card-title">Buscar usuario</h1>
					</div>
					<div class="card-body">
						{!! Form::open(['method'=>'get']) !!}
						<div class="form-row">
							<div class="form-group col">
								<label for="">Búsqueda</label>
								{!! Form::text('term', NULL, ['class'=>'form-control', 'id'=>'term']) !!}
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label for="">Mostrar usuarios: </label>
								<div class="btn-group" role="group" aria-label="Basic example">
									<button type="submit" class="btn btn-{{ $activos['todos'] }}" name="todos">Todos</button>
									<button type="submit" class="btn btn-{{ $activos['activo'] }}" name="activo">Activos</button>
									<button type="submit" class="btn btn-{{ $activos['inactivo'] }}" name="inactivo">Inactivos</button>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<a href="{{ route('usuarios.create') }}" class="btn btn-primary">Agregar usuario</a>
					</div>
					<div class="card-body table-responsive p-0">
						<table class="table">
							<thead>
								<tr>
									<td>#</td>
									<td>Nombre</td>
									<td>Usuario</td>
									<td>Email</td>
									<td>Estatus</td>
									<td>Roles</td>
									<td>Programas</td>
									<td>Opciones</td>
								</tr>
							</thead>
							<tbody>
								@forelse($users as $user)
									<tr>
										<td style="padding-top: 1.12rem">{{ $loop->iteration }}</td>
										<td style="padding-top: 1.12rem">{{ $user->name }}</td>
										<td style="padding-top: 1.12rem">{{ $user->username }}</td>
										<td style="padding-top: 1.12rem">{{ $user->email }}</td>
										<td>
											<div class="btn-group" role="group" aria-label="Basic example">
												<button type="submit" form="form-activar-{{ $user->id }}" class="btn btn-{{$user->statusActiveClass}}">Activo</button>
												<button type="submit" form="form-desactivar-{{ $user->id }}" class="btn btn-{{$user->statusDeactiveClass}}">Inactivo</button>
											</div>
											{!! Form::open(['route'=>['usuarios.activar', $user->id], 'method'=>'patch', 'id'=>'form-activar-'.$user->id]) !!}
											{!! Form::close() !!}
											{!! Form::open(['route'=>['usuarios.desactivar', $user->id], 'method'=>'delete', 'id'=>'form-desactivar-'.$user->id]) !!}
											{!! Form::close() !!}
										</td>
										<td style="padding-top: 1.12rem">{{ $user->groupsTags }}</td>
										<td style="padding-top: 1.12rem">{{ $user->programasTags }}</td>
										<td width="1%">
											<a href="{{ route('usuarios.edit', $user->id) }}" class="btn btn-link">
												Editar
											</a>
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="8"><h4>Sin resultados</h4></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop