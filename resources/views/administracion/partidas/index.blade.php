@extends('layouts.app')

@section('extra-scripts')

@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Partidas presupuestales</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
@stop

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card card-primary card-outline">
					<div class="card-body">
						{!! Form::open(['route'=>['partidas.store'],  'method'=>'post']) !!}
						<div class="col">
							<label for="">Nueva partida</label>
							{!! Form::text('descripcion', NULL, ['class'=>'form-control', 'required']) !!}
						</div>
						{!! Form::close() !!}
					</div>
					<hr class="m-0">
					<div class="card-body table-responsive p-0">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Partida</th>
									<th>Eliminar</th>
								</tr>
							</thead>
							<tbody>
								@forelse($partidas as $partida)
									<tr>
										<td width="1%">{{ $loop->iteration }}</td>
										<td width="">
											{!! Form::model($partida, ['route'=>['partidas.update', $partida->id], 'method'=>'patch']) !!}
											<div class="form-row">
												<div class="form-group col">
													{!! Form::text('descripcion', NULL, ['class'=>'form-control', 'required']) !!}
												</div>
											</div>
											{!! Form::close() !!}
										</td>
										<td width="1%">
											{!! Form::open(['route'=>['partidas.delete', $partida->id], 'class'=>'form-horizontal', 'method'=>'delete']) !!}
											<button class="btn btn-link" type="submit">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
												Eliminar
											</button>
											{!! Form::close() !!}
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="3"><h4>Sin resultados</h4></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop