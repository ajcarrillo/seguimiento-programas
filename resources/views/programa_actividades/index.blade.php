@extends('layouts.app')

@section('extra-scripts')
	@routes
	<script src="{{ mix('js/programa_actividades.js') }}" defer></script>
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Programa actividades</h1>
			</div>
		</div>
	</div>
@stop

@section('content')
	<section id="app">
		<index :progr="{{ json_encode($programa) }}"></index>
	</section>
@stop