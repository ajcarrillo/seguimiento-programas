@extends('layouts.app')

@section('extra-scripts')
	@routes
	<script src="{{ mix('js/start.js') }}" defer></script>
	<link rel="stylesheet" href="{{ mix('css/circle.css') }}">
@stop

@section('content-header')
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Programas</h1>
			</div><!-- /.col -->
		{{--<div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active">Dashboard v3</li>
			</ol>
		</div>--}}<!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
@stop

@section('content')
	<section id="app">
		<start></start>
	</section>
@stop