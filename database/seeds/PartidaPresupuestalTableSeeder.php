<?php

use Illuminate\Database\Seeder;

class PartidaPresupuestalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Acuerdos\Models\PartidaPresupuestal::create([
            'descripcion' => 'Prototipo para impresión de materiales',
        ]);
    }
}
