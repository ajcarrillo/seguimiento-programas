<?php

use Illuminate\Database\Seeder;

class NivelEducativoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $niveles = [
            'Preescolar',
            'Primaria',
            'Secundaria',
            'Preparatoria',
            'Educación Especial',
        ];

        foreach ($niveles as $nivel) {
            \Acuerdos\Models\NivelEducativo::create([
                'descripcion' => $nivel,
            ]);
        }
    }
}
