<?php

use Illuminate\Database\Seeder;

class TipoApoyoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programa = \Acuerdos\Models\Programa::where('descripcion', 'Programa Nacional de Convivencia Escolar')->first();

        \Acuerdos\Models\TipoApoyo::create([
            'programa_id' => $programa->id,
            'descripcion' => 'Materiales educativos a favor de la Convivencia Escolar',
        ]);

        \Acuerdos\Models\TipoApoyo::create([
            'programa_id' => $programa->id,
            'descripcion' => 'Apoyo financiero',
        ]);

        \Acuerdos\Models\TipoApoyo::create([
            'programa_id' => $programa->id,
            'descripcion' => 'Gastos de operación',
        ]);
    }
}
