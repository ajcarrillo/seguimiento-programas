<?php

use Illuminate\Database\Seeder;

class ProgramaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Acuerdos\Models\Programa::class, 15)->create();
        factory(\Acuerdos\Models\Programa::class)->create([
            'descripcion'              => 'Programa Nacional de Convivencia Escolar',
            'area_responsable_id'      => factory(\Acuerdos\Models\AreaResponsable::class)->create([
                'descripcion' => 'Coordinación General deEducación Básica',
            ]),
            'responsable_id'           => factory(\Acuerdos\Models\Responsable::class)->create([
                'descripcion' => 'Prof. Rafael A. Pantoja sánchez',
            ]),
            'area_operativa_id'        => factory(\Acuerdos\Models\AreaResponsable::class)->create([
                'descripcion' => 'Departamento de Convivencia Escolar',
            ]),
            'responsable_operativo_id' => factory(\Acuerdos\Models\Responsable::class)->create([
                'descripcion' => 'Mtra. Raquel Garbutt Mis / Dra. Antonia Morales Porcel',
            ]),
            'presupuesto'              => 0,
        ]);
    }
}
