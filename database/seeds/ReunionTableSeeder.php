<?php

use Illuminate\Database\Seeder;

class ReunionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Acuerdos\Models\Reunion::class, 10)->create()->each(function ($r) {
            $r->acuerdos()->save(factory(\Acuerdos\Models\Acuerdo::class)->make());
            $r->acuerdos()->save(factory(\Acuerdos\Models\Acuerdo::class)->make());
            $r->acuerdos()->save(factory(\Acuerdos\Models\Acuerdo::class)->make());
        });
    }
}
