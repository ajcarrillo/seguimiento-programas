<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupos = [
            'supermario',
            'admin',
            'admin_programa',
        ];

        foreach ($grupos as $grupo) {
            \Acuerdos\Group::create([
                'descripcion' => $grupo,
            ]);
        }
    }
}
