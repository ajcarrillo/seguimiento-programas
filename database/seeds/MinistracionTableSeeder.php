<?php

use Illuminate\Database\Seeder;

class MinistracionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programa = \Acuerdos\Models\Programa::where('descripcion', 'Programa Nacional de Convivencia Escolar')->first();

        \Acuerdos\Models\Ministracion::create([
            'programa_id'      => $programa->id,
            'presupuesto'      => 877910.00,
            'ingreso_sefiplan' => '2018-04-23',
            'ingreso_seq'      => '2018-05-15',
            'observaciones'    => '',
        ]);

        \Acuerdos\Models\Ministracion::create([
            'programa_id'      => $programa->id,
            'presupuesto'      => 1675400.00,
            'ingreso_sefiplan' => NULL,
            'ingreso_seq'      => NULL,
            'observaciones'    => 'No se radica al estado. Es para la impresión y distribución de materiales educativos',
        ]);

        \Acuerdos\Models\Ministracion::create([
            'programa_id'      => $programa->id,
            'presupuesto'      => 607822.00,
            'ingreso_sefiplan' => NULL,
            'ingreso_seq'      => NULL,
            'observaciones'    => 'Programado radicar en septiembre',
        ]);
    }
}
