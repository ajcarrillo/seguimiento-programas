<?php

use Illuminate\Database\Seeder;

class ActividadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoApoyo = \Acuerdos\Models\TipoApoyo::where('descripcion', 'Materiales educativos a favor de la Convivencia Escolar')->first();

        \Acuerdos\Models\Actividad::create([
            'programa_tipo_apoyo_id' => $tipoApoyo->id,
            'activdad'               => 'Distribución del material educativo a favor de la Convivencia Escolar',
            'proposito'              => 'Promover la intervención pedagógica en escuelas de educación básica a través de material educativo',
            'tipo_accion'            => 'Distribución de cuaderno de actividades, guías para el docente, manual de trabajo para padres de familia, cuaderno de actividades para el estudiante y pósters',
            'presupuesto_estimado'   => 1675400.00,
            'presupuesto_ejercido'   => 0.0,
            'observaciones'          => 'No se radica al estado. Es para la impresión y distribución de materiales educativos',
        ]);
    }
}
