<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GroupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(NivelEducativoTableSeeder::class);
        $this->call(BeneficiarioTableSeeder::class);
        $this->call(PartidaPresupuestalTableSeeder::class);

        if (env('APP_DEBUG') == true) {
            $this->call(ProgramaTableSeeder::class);
            $this->call(TipoApoyoTableSeeder::class);
            $this->call(ActividadTableSeeder::class);
            $this->call(MinistracionTableSeeder::class);
            $this->call(MetaTableSeeder::class);
            $this->call(ReunionTableSeeder::class);
        }
    }
}
