<?php

use Illuminate\Database\Seeder;

class BeneficiarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $beneficiarios = [
            'Escuelas', 'Alumnos', 'Alumnas', 'Docentes', 'Personal administrativo',
        ];

        foreach ($beneficiarios as $beneficiario) {
            factory(\Acuerdos\Models\Beneficiario::class)->create([
                'descripcion' => $beneficiario,
            ]);
        }
    }
}
