<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Acuerdos\User::class)->create([
            'name'           => 'Andrés Carrillo',
            'email'          => 'andresjch2804@gmail.com',
            'username'       => 'ajcarrillo',
            'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(60),
        ]);

        factory(\Acuerdos\User::class)->create([
            'name'           => 'Victor yah',
            'email'          => 'victor@gmail.com',
            'username'       => 'victor',
            'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(60),
        ]);

        $u = \Acuerdos\User::first();
        $u->groups()->attach(\Acuerdos\Group::pluck('id'));

        $u2 = \Acuerdos\User::where('username', 'victor')->first();
        $u2->groups()->attach(\Acuerdos\Group::pluck('id'));
    }
}
