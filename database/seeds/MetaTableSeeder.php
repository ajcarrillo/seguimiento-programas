<?php

use Illuminate\Database\Seeder;

class MetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actividad     = \Acuerdos\Models\Actividad::first();
        $beneficiarios = \Acuerdos\Models\Beneficiario::all();

        foreach ($beneficiarios as $beneficiario) {
            \Acuerdos\Models\Meta::create([
                'programa_tipo_apoyo_actividad_id' => $actividad->id,
                'beneficiario_id'                  => $beneficiario->id,
                'partida_presupuestal_id'          => \Acuerdos\Models\PartidaPresupuestal::inRandomOrder()->first()->id,
                'programada'                       => 250,
                'alcanzada'                        => 100,
                'fecha_inicio'                     => '2018-01-01',
                'fecha_fin'                        => '2018-01-01',
                'avance'                           => '30',
            ]);
        }
    }
}
