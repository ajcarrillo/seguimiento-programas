<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 02/07/18
 * Time: 22:31
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Responsable::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name,
    ];
});