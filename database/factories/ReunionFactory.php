<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/07/18
 * Time: 22:11
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Reunion::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->realText(140),
        'fecha'       => $faker->date(),
    ];
});