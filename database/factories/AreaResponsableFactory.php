<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 02/07/18
 * Time: 22:11
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\AreaResponsable::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->company,
    ];
});