<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 02/07/18
 * Time: 22:10
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Beneficiario::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name,
    ];
});