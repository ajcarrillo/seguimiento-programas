<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/07/18
 * Time: 22:27
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Invitado::class, function ($faker) {
    return [
        'acuerdo_id' => factory(\Acuerdos\Models\Acuerdo::class)->create(),
        'area_id'    => factory(\Acuerdos\Models\AreaResponsable::class)->create(),
        'user_id'    => factory(\Acuerdos\User::class)->create(),
    ];
});