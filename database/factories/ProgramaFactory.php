<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 04/07/18
 * Time: 12:02
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Programa::class, function (Faker $faker) {
    return [
        'descripcion'              => $faker->company,
        'area_responsable_id'      => factory(\Acuerdos\Models\AreaResponsable::class)->create(),
        'responsable_id'           => factory(\Acuerdos\Models\Responsable::class)->create(),
        'area_operativa_id'        => factory(\Acuerdos\Models\AreaResponsable::class)->create(),
        'responsable_operativo_id' => factory(\Acuerdos\Models\Responsable::class)->create(),
        'presupuesto'              => 0,
    ];
});
