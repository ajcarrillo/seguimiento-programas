<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/07/18
 * Time: 22:24
 */

use Faker\Generator as Faker;

$factory->define(\Acuerdos\Models\Acuerdo::class, function (Faker $faker) {
    return [
        'reunion_id'    => factory(\Acuerdos\Models\Reunion::class)->create(),
        'descripcion'   => $faker->realText(),
        'fecha_inicio'  => $faker->date(),
        'fecha_fin'     => $faker->date(),
        'observaciones' => $faker->paragraph,
    ];
});