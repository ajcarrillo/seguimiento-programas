<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunionAcuerdoInvitadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reunion_acuerdo_invitados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('acuerdo_id');
            $table->foreign('acuerdo_id')->references('id')->on('reunion_acuerdos');
            $table->unsignedSmallInteger('area_id');
            $table->foreign('area_id')->references('id')->on('areas_responsables');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedTinyInteger('avance')->nullable();
            $table->boolean('done')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reunion_acuerdo_invitados');
    }
}
