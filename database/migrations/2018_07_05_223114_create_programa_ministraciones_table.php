<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaMinistracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_ministraciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programa_id');
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->decimal('presupuesto', 11, 2);
            $table->date('ingreso_sefiplan')->nullable();
            $table->date('ingreso_seq')->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_ministraciones');
    }
}
