<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaActividadMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_tipo_apoyo_actividad_metas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programa_tipo_apoyo_actividad_id');
            $table->foreign('programa_tipo_apoyo_actividad_id', 'ptaam_ptaa_id_foreign')->references('id')->on('programa_tipo_apoyo_actividades');
            $table->unsignedInteger('beneficiario_id');
            $table->foreign('beneficiario_id', 'ptaam_b_id_foreign')->references('id')->on('beneficiarios');
            $table->unsignedInteger('partida_presupuestal_id');
            $table->foreign('partida_presupuestal_id', 'ptaam_pp_id_foreign')->references('id')->on('partidas_presupuestales');
            $table->unsignedInteger('programada');
            $table->unsignedInteger('alcanzada')->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->unsignedInteger('avance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_tipo_apoyo_actividad_metas');
    }
}
