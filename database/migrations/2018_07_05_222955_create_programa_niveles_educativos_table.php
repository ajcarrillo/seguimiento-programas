<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaNivelesEducativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_niveles_educativos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programa_id');
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->unsignedTinyInteger('nivel_educativo_id');
            $table->foreign('nivel_educativo_id')->references('id')->on('niveles_educativos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_niveles_educativos');
    }
}
