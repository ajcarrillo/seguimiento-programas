<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_tipo_apoyo_actividades', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programa_tipo_apoyo_id');
            $table->foreign('programa_tipo_apoyo_id')->references('id')->on('programa_tipos_apoyo');
            $table->text('activdad');
            $table->text('proposito');
            $table->text('tipo_accion');
            $table->decimal('presupuesto_estimado', 11, 2);
            $table->decimal('presupuesto_ejercido', 11, 2);
            $table->text('observaciones')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_tipo_apoyo_actividades');
    }
}
