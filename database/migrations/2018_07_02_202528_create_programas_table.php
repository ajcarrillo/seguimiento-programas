<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->unsignedSmallInteger('area_responsable_id');
            $table->foreign('area_responsable_id')->references('id')->on('areas_responsables');
            $table->unsignedInteger('responsable_id');
            $table->foreign('responsable_id')->references('id')->on('responsables');
            $table->unsignedSmallInteger('area_operativa_id');
            $table->foreign('area_operativa_id')->references('id')->on('areas_responsables');
            $table->unsignedInteger('responsable_operativo_id');
            $table->foreign('responsable_operativo_id')->references('id')->on('responsables');
            $table->decimal('presupuesto', 11, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas');
    }
}
