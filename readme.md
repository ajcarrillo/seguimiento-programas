# Seguimiento de Programas Federales

# Contribuir

:+1: :tada: Primero que nada gracias por tomarte el tiempo para contribuir a este proyecto :+1: :tada:

### Antes de hacer tu contribución por favor lee la guía de estilo del código [Guía de estilo del código](https://gitlab.com/ajcarrillo/guia_desarrollo/).
El estilo del código es especialmente importante si estamos en un equipo de desarrollo o si nuestro proyecto lo van a usar en algún momento otros desarrolladores. Pero, cuando trabajamos en un proyecto propio, también es una buena costumbre usar un estilo de código claro y optimizado. Nos ayudará a revisar mejor el código y a entenderlo si en algún momento tenemos que modificarlo o queremos reutilizarlo.

Para contribuir a este proyecto sigue los siguientes pasos:
 
 ```
 $ git clone https://gitlab.com/ajcarrillo/seguimiento-programas
 $ git checkout -b devTuNombre
 
 // Antes de subir al repositorio
 $ git pull origin master
 // Corrige errores, si los hay
 $ npm run prod
 $ git push origin devTuNombre
 ```
 
 * Clona el proyecto
 * Crea tu rama de trabajo
 * Haz tu contribución
 * Baja los últimos cambios en la rama `master`, arregla conflictos si los hay
 * Compila los assets con `$ npm run prod`
 * Sube tu contribución
 * Y haz un pull request a la rama `master` del proyecto.
 
## Dependencias

* `node`
* `yarn`

Para instalar `yarn` ejecuta en la terminal:

```
$ npm install --global yarn
```

## :baby_bottle: Baby Steps I

* Renombrar los archivos `/.env.example` a `.env`.
* Ejecutar en la linea de comandos `$ yarn install`.
* Ejecutar en la linea de comandos `$ composer install`
* Ejecutar en la linea de comandos `$ npm run dev`

### Archivo .env

Aquí se encuentra contenidas algunas variables y configuraciones de vital importancia para que el proyecto funcione.

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:

```
$ php artisan key:generate
```

## :baby_bottle: Baby Steps II

### Configuración y creacion de base de datos.

Puedes crear la base de datos ejecutando el siguiente script:

```sql
DROP DATABASE IF EXISTS acuerdos;
CREATE DATABASE acuerdos
   CHARACTER SET = 'utf8'
   COLLATE = 'utf8_general_ci';
```

*Nota: una vez creada la base de datos, configura tu conexión*

### Configuración de la conexión

```
DB_CONNECTION=mysql
DB_HOST=YourDatabaseServer
DB_PORT=YourPort
DB_DATABASE=acuerdos
DB_USERNAME=YourUsername
DB_PASSWORD=YourPassword
```

### Migraciones con laravel

Ejecutar los siguientes comandos para aplicar las migraciones e inicializar la base datos.

```
$ php artisan migrate:fresh --seed
```

## :baby_bottle: Baby Steps III

### Archivos estáticos

Los archivos estáticos como `js` y `css` se guardan en `resources/assets` colocando cada archivo en su carpeta correspondiente según el tipo de archivo. Para mover los archivos 
a la carpeta `public` debes configurar el archivo `webpack.mix.js` y ejecutar en la consola el siguiente comando:

``` 
$ npm run dev
```

Haz click en [Laravel Mix](https://laravel.com/docs/5.5/mix) para saber más acerca de esta configuración.

## Producción

Es importante cambiar en el archivo `.env` la variable `DEBUGBAR_ENABLED`:

```
DEBUGBAR_ENABLED = false
```

Esto para que la barra de depuración no se muestre.

# Compilar archivos estáticos en producción:

Este proyecto NO COMPILA assets en producción, se compilan directamente en local con siguiente comando:

``` 
$ npm run prod
```

Este proyecto NO INSTALA dependencias en producción, por lo tanto no hay que ejecutar `$ yarn install`

Happy coding!!! :slight_smile: :upside_down:

P.S. Alimentate sanamente, come frutas y verduras. :tomato: :hot_pepper: :corn: