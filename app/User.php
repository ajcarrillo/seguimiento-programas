<?php

namespace Acuerdos;

use Acuerdos\Models\Programa;
use Acuerdos\Models\Token;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'activo', 'api_token'
    ];

    protected $appends = [
        'groupsTags', 'status', 'statusActiveClass', 'statusDeactiveClass', 'programasTags',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tokens()
    {
        return $this->hasMany(Token::class, 'user_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_user', 'user_id', 'group_id');
    }

    public function programas()
    {
        return $this->belongsToMany(Programa::class, 'usuario_programa', 'usuario_id', 'programa_id');
    }

    public function hasRole($role)
    {
        if ( ! is_array($role)) {
            return $this->groups()->where('descripcion', $role)->count();
        }

        $rolesDelUsuario = $this->groups->pluck('descripcion')->toArray();

        if (count(array_intersect($rolesDelUsuario, $role))) {
            return true;
        }

        return false;
    }

    public function getStatusAttribute()
    {
        return $this->activo == true ? 'Activo' : 'Inactivo';
    }

    public function getGroupsTagsAttribute()
    {
        $groups = $this->groups()->get();

        return $groups->implode('descripcion', ', ');
    }

    public function getStatusActiveClassAttribute()
    {
        return $this->activo == true ? 'success disabled' : 'secondary';
    }

    public function getStatusDeactiveClassAttribute()
    {
        return $this->activo == true ? 'secondary' : 'success disabled';
    }

    public function getProgramasTagsAttribute()
    {
        $groups = $this->programas()->get();

        return $groups->implode('descripcion', ', ');
    }
}
