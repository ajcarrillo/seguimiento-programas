<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Ministracion;
use DB;
use Illuminate\Http\Request;

class ActualizaPresupuestoMinistracionController extends Controller
{
    public function update(Request $request, Ministracion $ministracion)
    {

        try {
            DB::transaction(function () use ($request, $ministracion) {
                $ministracion->update([
                    'presupuesto' => $request->input('presupuesto'),
                ]);
                $programa              = $ministracion->programa;
                $programa->presupuesto = $programa->ministraciones()->sum('presupuesto');
                $programa->save();
            });
            flash('El presupuesto se actualizó correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un erro al intentar actualizar el presupuesto')->success();
        } catch (\Throwable $e) {
            flash('Ha ocurrido un erro al intentar actualizar el presupuesto')->success();
        }

        return back();
    }
}
