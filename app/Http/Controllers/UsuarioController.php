<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Group;
use Acuerdos\Models\Programa;
use Acuerdos\User;
use DB;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    protected $activos = [
        'activo'   => 'secondary',
        'inactivo' => 'secondary',
        'todos'    => 'success',
    ];

    public function index(Request $request)
    {
        $query = User::with('groups')->orderBy('name')
            ->where('id', '<>', $request->user()->id);

        if ( ! $request->user()->hasRole('supermario')) {
            $query->whereRaw('id NOT IN ( SELECT subgu.user_id FROM group_user subgu WHERE subgu.group_id = 1 )');
        }

        if ($request->filled('term')) {
            $query->orWhere('name', 'like', '%' . $request->query('term') . '%');
            $query->orWhere('username', 'like', '%' . $request->query('term') . '%');
            $query->orWhere('email', 'like', '%' . $request->query('term') . '%');
        }

        if ( ! $request->has('todos')) {
            if ($request->has('activo')) {
                $query->where('activo', 1);
                $this->activos['activo'] = 'success';
                $this->activos['todos']  = 'secondary';
            }

            if ($request->has('inactivo')) {
                $query->where('activo', 0);
                $this->activos['inactivo'] = 'success';
                $this->activos['todos']    = 'secondary';
            }
        }

        $activos = $this->activos;
        $users   = $query->get();

        return view('administracion.usuarios.index', compact('users', 'activos'));
    }

    public function create()
    {
        $groups    = $this->getGrupos();
        $programas = $this->getProgramas();
        $isCreate  = true;

        return view('administracion.usuarios.create', compact('groups', 'isCreate', 'programas'));
    }

    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $user = User::create([
                    'name'      => $request->input('name'),
                    'username'  => $request->input('username'),
                    'email'     => $request->input('email'),
                    'password'  => bcrypt($request->input('password')),
                    'api_token' => str_random(60),
                ]);
                $user->groups()->sync($request->groups);
                $user->programas()->sync($request->programas);

                flash('El usuario se guardó correctamente')->success();
            });
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            flash('Ha ocurrido un error al intentar guardar el usuario, intente de nuevo')->error();
        } catch (\Throwable $e) {
            \Log::info($e->getMessage());
            flash('Ha ocurrido un error al intentar guardar el usuario, intente de nuevo')->error();
        }

        return back();
    }

    public function edit(User $user)
    {
        $groups    = $this->getGrupos();
        $programas = $this->getProgramas();
        $user->loadMissing('groups');
        $isCreate = false;

        return view('administracion.usuarios.edit', compact('user', 'groups', 'isCreate', 'programas'));
    }

    public function update(Request $request, User $user)
    {
        try {
            DB::transaction(function () use ($request, $user) {
                $user->update($request->input());
                $user->groups()->sync($request->groups);
                $user->programas()->sync($request->programas);
                flash('El usuario se guardó correctamente')->success();
            });
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            flash('Ha ocurrido un error al intentar guardar el usuario, intente de nuevo')->error();
        } catch (\Throwable $e) {
            \Log::info($e->getMessage());
            flash('Ha ocurrido un error al intentar guardar el usuario, intente de nuevo')->error();
        }

        return back();
    }

    public function delete(User $user)
    {
        $user->update([ 'activo' => false ]);
        flash('El usuario se eleminó correctamente')->success();

        return back();
    }

    protected function getGrupos()
    {
        $query = Group::orderBy('descripcion');

        if ( ! currentUser()->hasRole('supermario')) {
            $query->where('descripcion', '!=', 'supermario');
        }

        return $query->pluck('descripcion', 'id');
    }

    protected function getProgramas()
    {
        return Programa::orderBy('descripcion')->pluck('descripcion', 'id');
    }
}
