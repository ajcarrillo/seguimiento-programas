<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Beneficiario;
use Illuminate\Http\Request;

class BeneficiarioController extends Controller
{
    public function index(Request $request)
    {
        $beneficiarios = Beneficiario::orderBy('descripcion')->get();

        return view('administracion.beneficiarios.index', compact('beneficiarios'));
    }

    public function store(Request $request)
    {
        $beneficiario = new Beneficiario($request->input());

        $beneficiario->save();

        flash('El beneficiario se guardó correctamente')->success();

        return back();
    }

    public function update(Request $request, Beneficiario $beneficiario)
    {
        $beneficiario->update($request->input());

        flash('El beneficiario se actualizó correctamente')->success();

        return back();
    }

    public function delete(Beneficiario $beneficiario)
    {
        try {
            $beneficiario->delete();
            flash('El beneficiario se eliminó correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un error al intentar eliminar el beneficiario')->error();
        }

        return back();
    }
}
