<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Ministracion;
use Acuerdos\Models\Programa;
use DB;
use Illuminate\Http\Request;

class ProgramaMinistracionController extends Controller
{
    public function index(Request $request, Programa $programa)
    {
        $ministraciones = $programa->ministraciones()->orderBy('created_at', 'ASC')->get();

        return view('programa_ministraciones.index', compact('programa', 'ministraciones'));
    }

    public function store(Request $request, Programa $programa)
    {
        try {
            DB::transaction(function () use ($request, $programa) {
                $ministracion = new Ministracion($request->input());
                $programa->ministraciones()->save($ministracion);
                $this->updatePrograma($programa);
            });
            flash('Los datos se guardaron correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un error al intentar guardar la ministración')->success();
        } catch (\Throwable $e) {
            flash('Ha ocurrido un error al intentar guardar la ministración')->success();
        }

        return back();
    }

    public function update(Request $request, Programa $programa, Ministracion $ministracion)
    {
        $ministracion->update($request->input());
        flash('Los datos se guardaron correctamente')->success();

        return back();
    }

    public function destroy(Programa $programa, Ministracion $ministracion)
    {
        try {
            $ministracion->delete();
            $this->updatePrograma($programa);
            flash('Los datos se eliminaron correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un error al intentar eliminar la ministración')->error();
        }

        return back();
    }

    protected function updatePrograma(Programa $programa)
    {
        $programa->presupuesto = $programa->ministraciones()->sum('presupuesto');
        $programa->save();
    }
}
