<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\User;
use Illuminate\Http\Request;

class ActivarUsuarioController extends Controller
{
    public function update(User $user)
    {
        $user->update([ 'activo' => true ]);
        flash('El usuario se ha activado correctamente')->success();
        return back();
    }

    public function delete(User $user)
    {
        $user->update([ 'activo' => false ]);
        flash('El usuario se ha desactivado correctamente')->success();
        return back();
    }
}
