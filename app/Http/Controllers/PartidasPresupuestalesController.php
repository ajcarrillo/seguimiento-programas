<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\PartidaPresupuestal;
use Illuminate\Http\Request;

class PartidasPresupuestalesController extends Controller
{
    public function index(Request $request)
    {
        $partidas = PartidaPresupuestal::orderBy('descripcion')->get();

        return view('administracion.partidas.index', compact('partidas'));
    }

    public function store(Request $request)
    {
        $partida = new PartidaPresupuestal($request->input());

        $partida->save();

        flash('La partida se guardó correctamente')->success();

        return back();
    }

    public function update(Request $request, PartidaPresupuestal $partida)
    {
        $partida->update($request->input());

        flash('La partida se actualizó correctamente')->success();

        return back();
    }

    public function delete(PartidaPresupuestal $partida)
    {
        try {
            $partida->delete();
            flash('La partida se eliminó correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un error al intentar eliminar la partida')->error();
        }

        return back();
    }
}
