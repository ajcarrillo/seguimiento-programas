<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\AreaResponsable;
use Illuminate\Http\Request;

class AreaResponsableController extends Controller
{
    public function index(Request $request)
    {
        $areas = AreaResponsable::orderBy('descripcion')->get();

        return view('administracion.areas.index', compact('areas'));
    }

    public function store(Request $request)
    {
        $area = new AreaResponsable($request->input());

        $area->save();

        flash('El área se guardó correctamente')->success();

        return back();
    }

    public function update(Request $request, AreaResponsable $area)
    {
        $area->update($request->input());

        flash('El área se actualizó correctamente')->success();

        return back();
    }

    public function delete(AreaResponsable $area)
    {
        try {
            $area->delete();
            flash('El área se eliminó correctamente')->success();
        } catch (\Exception $e) {
            flash('Ha ocurrido un error al intentar eliminar el área')->error();
        }

        return back();
    }
}
