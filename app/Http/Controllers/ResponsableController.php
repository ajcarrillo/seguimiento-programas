<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Responsable;
use Illuminate\Http\Request;

class ResponsableController extends Controller
{
    public function index()
    {
        $responsables = Responsable::orderBy('descripcion')->get();

        return view('administracion.responsables.index', compact('responsables'));
    }

    public function store(Request $request)
    {
        $resposable = new Responsable($request->input());

        $resposable->save();

        flash('El responsable se guardó correctamente')->success();

        return back();
    }

    public function update(Request $request, Responsable $responsable)
    {
        $responsable->update($request->input());

        flash('El responsable se actualizó correctamente')->success();

        return back();
    }

    public function delete(Responsable $responsable)
    {
        try {
            $responsable->delete();
            flash('El responsable se eliminó correctamente')->success();
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            flash('Ha ocurrido un error al intentar eliminar el responsable')->error();
        }

        return back();
    }
}
