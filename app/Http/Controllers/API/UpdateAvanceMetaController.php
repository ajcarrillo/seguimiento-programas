<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Meta;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class UpdateAvanceMetaController extends Controller
{
    use ResponseTrait;

    public function update(Request $request, Meta $meta)
    {
        $meta->update([
            'avance' => $request->input('avance'),
        ]);

        $meta->loadMissing('beneficiario', 'partidaPresupuestal');

        return $this->respondWithArray(compact('meta'));
    }
}
