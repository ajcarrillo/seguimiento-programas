<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Responsable;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ResponsableController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $responsables = Responsable::orderBy('descripcion')->get([ 'id', 'descripcion' ]);

        return $this->respondWithArray(compact('responsables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
