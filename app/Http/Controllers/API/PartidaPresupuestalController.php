<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\PartidaPresupuestal;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class PartidaPresupuestalController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $partidas = PartidaPresupuestal::orderBy('descripcion')->get();

        return $this->respondWithArray(compact('partidas'));
    }
}
