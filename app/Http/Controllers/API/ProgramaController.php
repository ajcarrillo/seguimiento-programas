<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Programa;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ProgramaController extends Controller
{
    use ResponseTrait;

    public function index(Request $request)
    {
        $user      = \Auth::guard('api')->user();
        $programas = $user->programas()->with('areaResponsable', 'responsable', 'areaOperativa', 'responsableOperativo', 'nivelesEducativos', 'ministraciones', 'tipoApoyos')
            ->orderBy('descripcion')->get();

        return $this->respondWithArray(compact('programas'));
    }

    public function store(Request $request)
    {
        $programa = new Programa($request->input('programa'));
        $programa->save();

        $programaId = $programa->id;

        $user = \Auth::guard('api')->user();
        $user->programas()->attach([$programaId]);

        $programa->loadMissing('areaResponsable', 'responsable', 'areaOperativa', 'responsableOperativo', 'nivelesEducativos', 'ministraciones', 'tipoApoyos');

        return $this->respondWithArray(compact('programa'));
    }

    public function show(Programa $programa)
    {
        $programa->loadMissing('tipoApoyos', 'tipoApoyos.actividades', 'tipoApoyos.actividades.metas', 'tipoApoyos.actividades.metas.beneficiario', 'tipoApoyos.actividades.metas.partidaPresupuestal');

        return $this->respondWithArray(compact('programa'));
    }

    public function update(Request $request, Programa $programa)
    {
        $programa->update($request->input('programa'));

        $programa->loadMissing('areaResponsable', 'responsable', 'areaOperativa', 'responsableOperativo', 'nivelesEducativos', 'ministraciones', 'tipoApoyos');

        return $this->respondWithArray(compact('programa'));
    }

    public function destroy($id)
    {
        //
    }
}
