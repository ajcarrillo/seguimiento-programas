<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Programa;
use Acuerdos\Models\TipoApoyo;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ProgramaTipoApoyoController extends Controller
{
    use ResponseTrait;

    public function store(Request $request, Programa $programa)
    {
        $tipo = new TipoApoyo($request->input());

        $programa->tipoApoyos()->save($tipo);

        $tipo->loadMissing('actividades', 'actividades.metas');

        return $this->respondWithArray(compact('tipo'));
    }
}
