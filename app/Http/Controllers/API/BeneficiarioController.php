<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Beneficiario;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class BeneficiarioController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $beneficiarios = Beneficiario::orderBy('descripcion')->get();

        return $this->respondWithArray(compact('beneficiarios'));
    }
}
