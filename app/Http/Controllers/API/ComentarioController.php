<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Comentario;
use Acuerdos\Models\Reunion;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ComentarioController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        //
    }

    public function store(Request $request, Reunion $reunion)
    {
        $comment = new Comentario([
            'comentario' => $request->input('comentario'),
            'user_id'    => currentUser('api')->id,
        ]);

        $reunion->comments()->save($comment);

        $this->loadMissing($comment);

        return $this->respondWithArray(compact('comment'));
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    protected function loadMissing(Comentario $comentario)
    {
        return $comentario->loadMissing('user');
    }
}
