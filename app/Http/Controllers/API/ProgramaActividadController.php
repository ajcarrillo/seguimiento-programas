<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Actividad;
use Acuerdos\Models\Programa;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ProgramaActividadController extends Controller
{
    use ResponseTrait;

    public function index(Programa $programa)
    {
        $programa->loadMissing('tipoApoyos', 'tipoApoyos.actividades', 'tipoApoyos.actividades.metas');
        $actividades = $programa->tipoApoyos;

        return $this->respondWithArray(compact('actividades'));
    }

    public function store(Request $request, Programa $programa)
    {
        $actividad = new Actividad($request->input('actividad'));
        $actividad->save();

        $actividad->loadMissing('metas', 'metas.beneficiario', 'metas.partidaPresupuestal');

        return $this->respondWithArray(compact('actividad'));
    }

    public function show(Programa $programa, Actividad $actividad)
    {
        $actividad->loadMissing('metas', 'metas.beneficiario', 'metas.partidaPresupuestal');

        return $this->respondWithArray(compact('actividad'));
    }

    public function update(Request $request, Programa $programa, Actividad $actividad)
    {
        $actividad->update($request->input());

        $actividad->loadMissing('metas', 'metas.beneficiario', 'metas.partidaPresupuestal');

        return $this->respondWithArray(compact('actividad'));
    }

    public function destroy($id)
    {

    }
}
