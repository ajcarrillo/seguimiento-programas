<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Actividad;
use Acuerdos\Models\Meta;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class MetaController extends Controller
{
    use ResponseTrait;

    public function store(Request $request, Actividad $actividad)
    {
        $meta = new Meta($request->input('meta'));
        $actividad->metas()->save($meta);
        $meta->loadMissing('beneficiario', 'partidaPresupuestal');

        return $this->respondWithArray(compact('meta'));
    }

    public function update(Request $request, Actividad $actividad, Meta $meta)
    {
        $meta->update($request->input('meta'));
        $meta->loadMissing('beneficiario', 'partidaPresupuestal');

        return $this->respondWithArray(compact('meta'));
    }
}
