<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Acuerdo;
use Acuerdos\Models\Reunion;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class AcuerdoController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        //
    }

    public function store(Request $request, Reunion $reunion)
    {
        $acuerdo = new Acuerdo($request->acuerdo);

        $reunion->acuerdos()->save($acuerdo);

        $this->loadMissing($acuerdo);

        return $this->respondWithArray(compact('acuerdo'));
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    protected function loadMissing(Acuerdo $acuerdo)
    {
        return $acuerdo->loadMissing('invitados', 'comments', 'comments.user');
    }
}
