<?php

namespace Acuerdos\Http\Controllers\API;

use Acuerdos\Models\Reunion;
use Acuerdos\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Acuerdos\Http\Controllers\Controller;

class ReunionController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $reuniones = Reunion::with('comments', 'acuerdos', 'acuerdos.invitados', 'acuerdos.comments')->orderBy('created_at', 'DESC')->get();

        return $this->respondWithArray(compact('reuniones'));
    }

    public function store(Request $request)
    {
        $reunion = new Reunion($request->reunion);

        $reunion->save();

        $this->loadMissing($reunion);

        return $this->respondWithArray(compact('reunion'));
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, Reunion $reunion)
    {
        $reunion->update($request->reunion);

        $this->loadMissing($reunion);

        return $this->respondWithArray(compact('reunion'));
    }

    public function destroy(Reunion $reunion)
    {
        try {
            $reunion->delete();

            return $this->sendSuccessResponse();
        } catch (\Exception $e) {

            return $this->sendConflictResponse('Ha ocurrido un erro al intentar eliminar la reunión');
        }
    }

    protected function loadMissing(Reunion $reunion)
    {
        return $reunion->loadMissing('comments', 'acuerdos', 'acuerdos.invitados', 'acuerdos.comments', 'acuerdos.comments.user');
    }
}
