<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Programa;
use Illuminate\Http\Request;

class ProgramaActividadController extends Controller
{
    public function index(Request $request, Programa $programa)
    {
        $programa->loadMissing('tipoApoyos', 'tipoApoyos.actividades', 'tipoApoyos.actividades.metas', 'tipoApoyos.actividades.metas.beneficiario', 'tipoApoyos.actividades.metas.partidaPresupuestal');
        return view('programa_actividades.index', compact('programa'));
    }
}
