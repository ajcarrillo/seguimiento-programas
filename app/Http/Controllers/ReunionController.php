<?php

namespace Acuerdos\Http\Controllers;

use Acuerdos\Models\Reunion;
use Illuminate\Http\Request;

class ReunionController extends Controller
{
    public function index()
    {
        $reuniones = Reunion::with('comments', 'acuerdos', 'acuerdos.invitados', 'acuerdos.comments', 'acuerdos.comments.user')
            ->orderBy('fecha', 'DESC')
            ->get();

        return view('reuniones.index', compact('reuniones'));
    }
}
