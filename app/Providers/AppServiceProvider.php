<?php

namespace Acuerdos\Providers;

use Acuerdos\Models\Acuerdo;
use Acuerdos\Models\Reunion;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::withDoubleEncoding();

        Relation::morphMap([
            'reuniones' => Reunion::class,
            'acuerdos' => Acuerdo::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
