<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/07/18
 * Time: 11:11
 */
function currentUser($type = NULL)
{
    if (is_null($type)) {
        return Auth::user();
    }

    return Auth::guard('api')->user();
}

function getUserGroups()
{
    $user = currentUser();

    return $user->groups()->pluck('descripcion')->toArray();
}

function menu()
{
    return [ 'menu' =>
                 [
                     'nombre'   => 'Programas',
                     'url'      => route('programas.index'),
                     'submenus' => [],
                     'groups'   => [ 'supermario', 'admin', 'admin_programa' ],
                 ],
        [
            'nombre'   => 'Configuraciones',
            'url'      => '',
            'submenus' => [
                [
                    'nombre' => 'Áreas',
                    'url'    => route('areas.index'),
                    'groups' => [ 'supermario', 'admin', 'admin_programa' ],
                ],
                [
                    'nombre' => 'Beneficiarios',
                    'url'    => route('beneficiarios.index'),
                    'groups' => [ 'supermario', 'admin', 'admin_programa' ],
                ],
                [
                    'nombre' => 'Partidas',
                    'url'    => route('partidas.index'),
                    'groups' => [ 'supermario', 'admin', 'admin_programa' ],
                ],
                [
                    'nombre' => 'Responsables',
                    'url'    => route('responsables.index'),
                    'groups' => [ 'supermario', 'admin', 'admin_programa' ],
                ],
                [
                    'nombre' => 'Usuarios',
                    'url'    => route('usuarios.index'),
                    'groups' => [ 'supermario', 'admin', 'admin_programa' ],
                ],
            ],
            'groups'   => [ 'supermario', 'admin', 'admin_programa' ],
        ],
        [
            'nombre'   => 'Reuniones',
            'url'      => route('reuniones.index'),
            'submenus' => [],
            'groups'   => [ 'supermario', 'admin' ],
        ],
    ];
}