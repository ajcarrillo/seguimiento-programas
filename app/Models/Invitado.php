<?php

namespace Acuerdos\Models;

use Acuerdos\User;
use Illuminate\Database\Eloquent\Model;

class Invitado extends Model
{
    protected $table   = 'reunion_acuerdo_invitados';
    protected $guarded = [];

    public function acuerdo()
    {
        return $this->belongsTo(Invitado::class, 'acuerdo_id');
    }

    public function area()
    {
        return $this->belongsTo(AreaResponsable::class, 'area_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
