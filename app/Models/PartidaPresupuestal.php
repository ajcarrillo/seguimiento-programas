<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class PartidaPresupuestal extends Model
{
    protected $table   = 'partidas_presupuestales';
    protected $guarded = [];

    public function metas()
    {
        return $this->hasMany(Meta::class, 'partida_presupuestal_id');
    }
}
