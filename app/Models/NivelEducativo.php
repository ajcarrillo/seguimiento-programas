<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class NivelEducativo extends Model
{
    protected $table   = 'niveles_educativos';
    protected $guarded = [];

    public function programas()
    {
        return $this->belongsToMany(Programa::class, 'programa_niveles_educativos', 'nivel_educativo_id', 'programa_id');
    }
}
