<?php

namespace Acuerdos\Models;

use Acuerdos\User;
use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    protected $table   = 'programas';
    protected $guarded = [];
    protected $appends = [
        'presupuesto_ejercido',
    ];

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'usuario_programa', 'programa_id', 'usuario_id');
    }

    public function areaResponsable()
    {
        return $this->belongsTo(AreaResponsable::class, 'area_responsable_id');
    }

    public function responsable()
    {
        return $this->belongsTo(Responsable::class, 'responsable_id');
    }

    public function areaOperativa()
    {
        return $this->belongsTo(AreaResponsable::class, 'area_operativa_id');
    }

    public function responsableOperativo()
    {
        return $this->belongsTo(Responsable::class, 'responsable_operativo_id');
    }

    public function nivelesEducativos()
    {
        return $this->belongsToMany(NivelEducativo::class, 'programa_niveles_educativos', 'programa_id', 'nivel_educativo_id');
    }

    public function ministraciones()
    {
        return $this->hasMany(Ministracion::class, 'programa_id');
    }

    public function tipoApoyos()
    {
        return $this->hasMany(TipoApoyo::class, 'programa_id');
    }

    public function getPresupuestoEjercidoAttribute()
    {
        $tiposApoyo = $this->tipoApoyos()->pluck('id');

        return Actividad::whereIn('programa_tipo_apoyo_id', $tiposApoyo)->sum('presupuesto_ejercido');
    }
}
