<?php

namespace Acuerdos\Models;

use Acuerdos\User;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table   = 'comentarios';
    protected $guarded = [];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
