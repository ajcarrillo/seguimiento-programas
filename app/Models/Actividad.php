<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table   = 'programa_tipo_apoyo_actividades';
    protected $guarded = [];

    public function tipoApoyo()
    {
        return $this->belongsTo(TipoApoyo::class, 'programa_tipo_apoyo_id');
    }

    public function metas()
    {
        return $this->hasMany(Meta::class, 'programa_tipo_apoyo_actividad_id');
    }
}
