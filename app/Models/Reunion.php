<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    protected $table = 'reuniones';
    protected $guarded = [];

    public function acuerdos()
    {
        return $this->hasMany(Acuerdo::class, 'reunion_id');
    }

    public function comments()
    {
        return $this->morphMany(Comentario::class, 'commentable')->with('user');
    }
}
