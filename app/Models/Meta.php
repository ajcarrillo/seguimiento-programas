<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table   = 'programa_tipo_apoyo_actividad_metas';
    protected $guarded = [];

    public function tipoApoyoActividad()
    {
        return $this->belongsTo(TipoApoyo::class, 'programa_tipo_apoyo_actividad_id');
    }

    public function beneficiario()
    {
        return $this->belongsTo(Beneficiario::class, 'beneficiario_id');
    }

    public function partidaPresupuestal()
    {
        return $this->belongsTo(PartidaPresupuestal::class, 'partida_presupuestal_id');
    }
}
