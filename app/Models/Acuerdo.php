<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Acuerdo extends Model
{
    protected $table   = 'reunion_acuerdos';
    protected $guarded = [];

    public function reunion()
    {
        return $this->belongsTo(Reunion::class, 'reunion_id');
    }

    public function invitados()
    {
        return $this->hasMany(Invitado::class, 'acuerdo_id');
    }

    public function comments()
    {
        return $this->morphMany(Comentario::class, 'commentable')->with('user');
    }
}
