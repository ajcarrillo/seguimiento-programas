<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class AreaResponsable extends Model
{
    protected $table   = 'areas_responsables';
    protected $guarded = [];

    public function programas()
    {
        return $this->hasMany(Programa::class, 'area_responsable_id');
    }

    public function programasOperativos()
    {
        return $this->hasMany(Programa::class, 'area_operativa_id');
    }
}
