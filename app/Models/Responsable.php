<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Responsable extends Model
{
    protected $table = 'responsables';
    protected $guarded = [];

    public function programas()
    {
        return $this->hasMany(Programa::class, 'responsable_id');
    }

    public function programasOperativos()
    {
        return $this->hasMany(Programa::class, 'responsable_operativo_id');
    }
}
