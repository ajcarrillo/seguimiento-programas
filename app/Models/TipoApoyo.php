<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class TipoApoyo extends Model
{
    protected $table   = 'programa_tipos_apoyo';
    protected $guarded = [];

    public function programa()
    {
        return $this->belongsTo(Programa::class, 'programa_id');
    }

    public function actividades()
    {
        return $this->hasMany(Actividad::class, 'programa_tipo_apoyo_id');
    }
}
