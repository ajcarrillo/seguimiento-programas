<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Beneficiario extends Model
{
    protected $table   = 'beneficiarios';
    protected $guarded = [];

    public function actividades()
    {
        return $this->hasMany(Actividad::class, 'beneficiario_id');
    }
}
