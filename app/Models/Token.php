<?php

namespace Acuerdos\Models;

use Acuerdos\User;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table   = 'tokens';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function generateFor(User $user)
    {
        return static::create([
            'token'   => str_random(60),
            'user_id' => $user->id,
        ]);
    }
}
