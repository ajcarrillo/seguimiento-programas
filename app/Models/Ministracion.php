<?php

namespace Acuerdos\Models;

use Illuminate\Database\Eloquent\Model;

class Ministracion extends Model
{
    protected $table   = 'programa_ministraciones';
    protected $guarded = [];

    public function programa()
    {
        return $this->belongsTo(Programa::class, 'programa_id');
    }
}
